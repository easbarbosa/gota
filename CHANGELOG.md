<!--
Gota is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gota is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Gota. If not, see <https://www.gnu.org/licenses/>.
-->

# CHANGELOG

## 0.4.0
- feat: add upgrade and refresh options 

## 0.3.0

- feat: use subprocess.run() to run commands
- feat: pack to symbolic link executable

## 0.2.2

- tests: add container,makefile and fix github action
- tests: essential tests, dockerfile, github action
- chore: feed PROTOCOL and improve README
- chore: examples configs to toml and readme
- feat: toml for configuration

## [0.1.0] - 27/02/2023

- initial commit
