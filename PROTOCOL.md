<!--
Gota is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gota is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Gota. If not, see <https://www.gnu.org/licenses/>.
-->

# PROTOCOL

## Actions

### Errors

- if root config folder is not found, exit with information
- if root config folder is found, but action folders has no config, exit with information

### Files

- Parse and run all configuration files in ACTION folder
- If `--filter NAME` is given, filter and parse those files matching
