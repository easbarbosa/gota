# Gota is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gota is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gota. If not, see <https://www.gnu.org/licenses/>.


from pathlib import Path
import unittest

from main import parse, toml, filtered, routes

examples = Path.home().joinpath(".config", "gota")
misc = examples.joinpath("misc")
distro = examples.joinpath("distro")
pack = examples.joinpath("pack")


class TestMain(unittest.TestCase):
    def test_parse_successfully(self) -> None:
        systemctl = misc.joinpath("systemctl.toml")
        with open(systemctl, "rb") as rawdata:
            foo = parse(rawdata)

        self.assertEqual(foo["command"], "sudo systemctl enable --now")

    def test_toml_only_files(self) -> None:
        files = toml(distro.iterdir())

        self.assertEqual(len(files), 2)

    def test_filtered_only_files(self) -> None:
        files = filtered(pack.iterdir(), "python")

        self.assertEqual(len(list(files)), 1)

    def test_routes_returns_misc_action(self) -> None:
        mm = str(routes("misc"))

        self.assertTrue(mm.startswith("<function misc"))


if __name__ == '__main__':
    unittest.main()
