# Gota is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gota is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gota. If not, see <https://www.gnu.org/licenses/>.

# DEPENDENCIES: awk, podman

.DEFAULT_GOAL := tests

RUNNER ?= podman
NAME := gota
VERSION := $(shell awk '/version/ {version=substr($$3, 2,5) ;print version}' ./pyproject.toml )
IMAGENAME := ${USER}/${NAME}:${VERSION}

# ======================================================

.PHONY: repl
tests:
	podman run --rm -it ${IMAGENAME}

.PHONY: image.build
image.build:
	${RUNNER} build --file ./Containerfile --tag ${IMAGENAME}

.PHONY: image.repl
image.repl:
	podman run --rm -it ${IMAGENAME} bash
